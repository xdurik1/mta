from flask import Flask, abort, jsonify, render_template, escape, flash, redirect, url_for, session, logging, request
from flask_cors import CORS
from flask_bootstrap import Bootstrap
from werkzeug.security import generate_password_hash, check_password_hash
from passlib.hash import sha256_crypt
from functools import wraps
from flask_datepicker import datepicker
import sys
import MTA
import Form

import re

from flask import Flask, redirect, url_for, render_template, session
from flask_wtf import FlaskForm
#from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired
from wtforms import validators, SubmitField

from flask_paginate import Pagination, get_page_args

import logging
logging.basicConfig(filename='file.log',level=logging.DEBUG, format='%(asctime)s %(levelname)s\n\r%(message)s', datefmt='%H:%M:%S')

app = Flask(__name__)
app.config['WTF_I18N_ENABLED'] = False
Bootstrap(app)
datepicker(app)

# TODO: Vypnut v produkcii!!!
# Response header Acces-Control-Allow-Origin: *
CORS(app, support_credentials=True)

################# DB CONNECTION ##################
connection = MTA.connect_db()

# Pred obsluzenim kazdeho poziadavku skontroluje spojenie s DB
# Ak je neaktivne, vytvori nove
@app.before_request
def check_connection():
    global connection # Globalna premenna
    if not connection or connection.closed:
        connection = MTA.connect_db()

################# DEKORATORY ##################
#zabezpecenie pred neopravnenym pristupom - dekorator
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap

#zabezpecenie pred neopravnenym pristupom neadmina
def is_admin(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'is_admin' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login in like admin', 'danger')
            return redirect(url_for('hello'))
    return wrap

def user_exist(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        exist = MTA.user_exist(connection=connection, login = session['login'])
        if exist:
            return f(*args, **kwargs)
        else:
            session.clear()
            flash('User was probably deleted', 'danger')
            return redirect(url_for('login'))
    return wrap

#################### PAGINATION FUNKCIE ####################
def get_pagination(offset=0, per_page=10):
        return result[offset: offset + per_page]

#################### ERROR HANDLER ####################
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'),404

# Wrapper pre vlastne error statusy
def error_status(error_msg=None, status_code=500):
    message = {
        'message': str(error_msg),
        'status_code': status_code
    }
    
    resp = jsonify(message)
    return resp

def check_validity_of_email(email):
    pattern = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    # pass the regular expression
    # and the string into the fullmatch() method
    if(re.fullmatch(pattern, email)):
        return True
 
    else:
        return False

#################### ROUTING ####################
@app.route('/xxx')
def indexxxxxx(): 
    return jsonify(session) 
@app.route('/multiple')
def indexxxx(): 
    return render_template('skuska_multiple_select.html')
 
@app.route("/ajax_add",methods=["POST","GET"])
def ajax_add():
    if request.method == 'POST':
        hidden_skills = request.form['hidden_skills']
        print(hidden_skills)     
        return hidden_skills        
    return "salslas"


#HOMEPAGE
@app.route('/')
def index():
    return render_template('home.html')

#LOGIN
@app.route('/login', methods=['GET','POST'])
def login():
    try:
        if request.method == 'POST':
            login = request.form['login']
            password_candidate = request.form['password']

            data = MTA.log_in(connection=connection, login = login, password_candidate = password_candidate)
            
            if data :
                if sha256_crypt.verify(password_candidate, data[0]):
                    session['logged_in'] = True
                
                    if data[1] == 2:
                        session['is_admin'] = True
                    login = MTA.get_user_by_email(connection=connection, login=login)
                    session['login'] = login['name']
                    session['email'] = login['email']
                    
                    flash('You are now logged in', 'success')
                    return redirect(url_for('hello')) 
                    
                else:
                    error = 'Invalid login or password'
                    return render_template('login.html', error = error)
                    
            else:
                error = 'User does not exist'
                return render_template('login.html', error = error)

        return render_template('login.html')
    except (RuntimeError, TypeError, NameError, Exception) as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        return render_template('login.html')

#REVIEWS
@app.route('/reviews', methods=['GET','POST'])
@is_logged_in
@user_exist
def get_reviews_features():
    try:
        features_text = MTA.get_all_features_text(connection=connection)
        form = Form.SearchForm2()
        #form.state.choices = [(f['feature_names_id'], f['text']) for f in features_text]

        email = session['email']
        id = MTA.get_id_by_email(connection=connection, email=email)
        
        submited_reviews = MTA.get_numbers_of_submited_reviews(connection=connection)
        suggested_reviews = MTA.get_numbers_of_suggested_reviews(connection=connection)
        reviews = MTA.get_numbers_of_reviews(connection=connection)
        numbers = MTA.get_numbers_of_reviews1(connection=connection, id= id['users_id'])
        
        result = []
        #vratim len kde su zamietnute recenzie
        if request.args.get('rejected'):
            #return jsonify(request.args)
            a = MTA.get_rejected_reviews(connection=connection, id_users=id['users_id'])
            result = a
        #vratim to kde su potvrdene recenzie
        if request.args.get('submited'):
            b = MTA.get_submited_reviews(connection=connection, id_users=id['users_id'])
            result= result + b
            #return jsonify(result)
        #vratim to kde su navrhnute recenzie
        if request.args.get('suggested'):
            c = MTA.get_suggested_reviews(connection=connection, id_users=id['users_id'])
            result= result + c
        #vratim to kde su vratene recenzie
        if request.args.get('returned'):
            d = MTA.get_returned_reviews_ext1(connection=connection, id_users=id['users_id'])
            result= result + d
            #return jsonify(d)
        #vratim to kde su schvalene recenzie
        if request.args.get('approved'):
            e = MTA.get_approved_reviews(connection=connection, id_users=id['users_id'])
            result= result + e
            #return jsonify(result)
        #vratim to kde su neskontrolovane recenzie
        if request.args.get('unchecked'):
            f = MTA.get_reviews_features3_without_empty_model(connection=connection, id = id['users_id'])
            result= result + f
        #vratim to kde su nedotknute recenzie
        if request.args.get('untouched'):
            g = MTA.get_untouched_reviews(connection=connection)
            #return jsonify(g)
            result = g

        #Cast spustana po odoslani formularu
        if request.args.get('search'):
            asd = []
            #result1 = MTA.example(connection=connection, filter = request.args.get('state'), value=request.args.get('search'))
            #return jsonify(result1)
            for r in result:
                if str(request.args.get('state')) == 'reviews_id':
                    if request.args.get('search') == str(r[request.args.get('state')]):
                        asd.append(r)
                else:
                    if request.args.get('search') in str(r[request.args.get('state')]):
                #if request.args.get('search') in str(r[request.args.get('state')]):
                #if str(r[request.args.get('state')]) == request.args.get('search'):
                        asd.append(r)
                    
            result = asd       
            #return jsonify(result)
        

        #return jsonify(asd)

        ''' PRE predcha dynamicky selectfield form
        if request.args.get('search'):
            #return jsonify(request.args.get('state'))
            if request.args.get('search') == 'feature': 
                asd = []
                for r in result:

                    if r['feature_names_id'] == int(request.args.get('state')):
                        asd.append(r)
                    
                result = asd       
                #return jsonify(result)
            elif request.args.get('search') == 'sentiment':
                asd = []
                for r in result:
                    if r['sentiment'] == request.args.get('state'):
                        asd.append(r)
                        
                result = asd       
                #return jsonify(result)
            #return jsonify(fil_result, result)
        '''

        #return jsonify(result)


        if not result and not request.args:
            result = MTA.get_reviews_default_extend_without_empty_model_extended(connection=connection, id = id['users_id'])

        def get_reviews(offset=0, per_page=10):
            return result[offset: offset + per_page]

        #return jsonify(result)
        page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
        total = len(result)
        pagination_users = get_reviews(offset=offset, per_page=per_page)
        #return jsonify(pagination_users)
        pagination = Pagination(page=page, per_page=per_page, total=total,
                                css_framework='bootstrap4') 
        #return jsonify(result)
        #return jsonify(pagination_users)
        return render_template('reviews3.html',
                            submited_reviews=submited_reviews,
                            reviews=reviews,
                            suggested_reviews=suggested_reviews,
                            users=pagination_users,
                            page=page,
                            per_page=per_page,
                            pagination=pagination, form=form)
    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#REVIEWS
@app.route('/reviews2', methods=['GET','POST'])
@is_logged_in
@user_exist
def get_reviews_features2():
    try:
        features_text = MTA.get_all_features_text(connection=connection)
        form = Form.SearchForm2()
        #form.state.choices = [(f['feature_names_id'], f['text']) for f in features_text]

        email = session['email']
        id = MTA.get_id_by_email(connection=connection, email=email)
        
        submited_reviews = MTA.get_numbers_of_submited_reviews(connection=connection)
        suggested_reviews = MTA.get_numbers_of_suggested_reviews(connection=connection)
        reviews = MTA.get_numbers_of_reviews(connection=connection)
        numbers = MTA.get_numbers_of_reviews1(connection=connection, id= id['users_id'])
        
        result = []
        #vratim len kde su zamietnute recenzie
        if request.method == 'POST':
            skills = request.form.getlist('skills')
            state = request.form.get('state')
            search = request.form.get('search')
            
            if 'Rejected' in skills:
                a = MTA.get_rejected_reviews(connection=connection, id_users=id['users_id'])
                result = a

            if 'Submited' in skills:
                b = MTA.get_submited_reviews(connection=connection, id_users=id['users_id'])
                result= result + b

            if 'Suggested' in skills:
                c = MTA.get_suggested_reviews(connection=connection, id_users=id['users_id'])
                result= result + c

            if 'Returned' in skills:
                d = MTA.get_returned_reviews_ext(connection=connection, id_users=id['users_id'])
                result= result + d

            if 'Approved' in skills:
                e = MTA.get_approved_reviews(connection=connection, id_users=id['users_id'])
                result= result + e

            if 'Unchecked' in skills:
                f = MTA.get_reviews_features3_without_empty_model(connection=connection, id = id['users_id'])
                result= result + f

            if 'Untouched' in skills:
                g = MTA.get_untouched_reviews(connection=connection)
                #return jsonify(g)
                result = g

            if search:
                temp_res = []

                for r in result:
                    if str(state) == 'reviews_id':
                        if search == str(r[state]):
                            temp_res.append(r)
                    else:
                        if search in str(r[state]):
                            temp_res.append(r)
                    
                result = temp_res       

        ''' PRE predcha dynamicky selectfield form
        if request.args.get('search'):
            #return jsonify(request.args.get('state'))
            if request.args.get('search') == 'feature': 
                asd = []
                for r in result:

                    if r['feature_names_id'] == int(request.args.get('state')):
                        asd.append(r)
                    
                result = asd       
                #return jsonify(result)
            elif request.args.get('search') == 'sentiment':
                asd = []
                for r in result:
                    if r['sentiment'] == request.args.get('state'):
                        asd.append(r)
                        
                result = asd       
                #return jsonify(result)
            #return jsonify(fil_result, result)
        '''

        #return jsonify(result)


        if not result and not request.args:
            result = MTA.get_reviews_default_extend_without_empty_model(connection=connection, id = id['users_id'])

        
        def get_reviews(offset=0, per_page=10):
            return result[offset: offset + per_page]

        #return jsonify(result)
        page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
        total = len(result)
        pagination_users = get_reviews(offset=offset, per_page=per_page)
        #return jsonify(pagination_users)
        pagination = Pagination(page=page, per_page=per_page, total=total,
                                css_framework='bootstrap4') 
        #return jsonify(result)
        #return jsonify(pagination_users)
        return render_template('reviews2.html',
                            submited_reviews=submited_reviews,
                            reviews=reviews,
                            suggested_reviews=suggested_reviews,
                            users=pagination_users,
                            page=page,
                            per_page=per_page,
                            pagination=pagination, form=form)
    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)
    

#SUBMIT REVIEW
@app.route('/submit/<string:id>', methods=['POST'])
@is_logged_in
@user_exist
def submit(id):
    try:
        email = session['email']
        a = request.referrer
        
        login_id = MTA.get_id_by_email(connection=connection, email=email)
        review = MTA.get_review_by_id(connection=connection, review_id=id)
        MTA.submit_review(connection=connection, review_id=id, user_id=login_id['users_id'], 
                          sentiment= review['sentiment'], feature_names_id = review['feature_names_id'])
       
        flash('Review was submited ', 'success')
        return redirect(a, code=302)
        return redirect(url_for('get_reviews_features'))

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#EDIT REVIEW
@app.route('/review/<int:id>', methods=['GET','POST'])
@is_logged_in
@user_exist
def review(id):
    try:
        #natiahnutie review
        result = MTA.get_review(connection=connection, id=id)
        resp = dict(result)
        prev = request.referrer

        product_categories_id = MTA.get_category_id(connection=connection, category=resp['czech_name'])
        product_categories_id = product_categories_id['product_categories_id']

        feature = resp['feature']

        language = resp['languages_id']
        
        #return jsonify(language)
        numbers_features = MTA.get_sug_fea_for_review(connection=connection, reviews_id = resp['reviews_id'])
        numbers_sentiment = MTA.get_sug_sent_for_review(connection=connection, reviews_id = resp['reviews_id'])

        suggestions = MTA.get_suggestions_for_review(connection=connection, reviews_id = resp['reviews_id'])

        #natiahnutie idciek a textu 
        #languages = MTA.get_language_by_feature(connection=connection, features_id= resp['reviews_id'])
        languages = MTA.get_languages(connection=connection)
        #nasypanie moznosti do SelectField-u
        groups_list1=[(l['languages_id'], l['title_english']) for l in languages]



        #natiahnutie idciek a textu 
        features_text = MTA.get_features_by_language(connection=connection, product_categories_id=product_categories_id, languages_id = language)
        #return jsonify(features_text)
        #nasypanie moznosti do SelectField-u
        #groups_list=[(f['feature_names_id'], f['text']) for f in features_text]
        groups_list=[(f['features_id'], f['text']) for f in features_text]

        #return jsonify (groups_list)
        form = Form.ReviewForm()

        for x in features_text:
            if x['text'] == feature:
                #a = x['feature_names_id']
                feature_id = x['features_id']
                b = x['feature_names_id']


        #for x in languages:
         #   if x['languages_id'] == language:
          #      aa = x['title_english']

        #return jsonify(b)

        language_of_feature = MTA.get_language_by_feature(connection=connection, features_id= b)['languages_id']#resp['reviews_id'])['languages_id']
        
        form.review_text.data = resp['text']
        form.language.choices = groups_list1
        form.category.data = resp['czech_name']
        form.sentiment.data = resp['sentiment']
        form.attribute.choices = groups_list

        if request.method =='POST': 
            if 'attribute' in request.form:
                if feature_id == int(request.form['attribute']) and resp['sentiment'] == request.form['sentiment'] and int(request.form['language']) == resp['languages_id']:
                    flash('No change has been designed', 'danger')
                    return render_template("modify_review1.html", form = form, numbers_features=numbers_features,
                               numbers_sentiment=numbers_sentiment, suggestions=suggestions)
                prev = session['previous']
                
                review_text = resp['text']
                form.category.data = resp['czech_name']
                sentiment = request.form['sentiment']
                attribute = request.form['attribute']
                language = request.form['language']
                email = session['email']

                features_names_id = MTA.get_feature_by_lang_fea_product_cat(connection=connection, features_id = attribute, languages_id = language, product_categories_id=product_categories_id)
                #features_names_id = features_names_id['feature_names_id']
                #return jsonify(features_names_id)

                id_login = MTA.get_id_by_email(connection=connection, email=email)
                id_login = id_login['users_id']

                MTA.add_suggestion(connection=connection, id_login=id_login, id=id, sentiment=sentiment, attribute=features_names_id['feature_names_id'])

                flash('Review Updated', 'success')
                return redirect(prev, code=302)
                #return redirect(url_for('get_reviews_features'))
            else:
                flash('Something is going wrong', 'danger')
                prev = request.referrer
                return redirect(prev, code=302)
            
        
        form.language.data = language_of_feature
        form.attribute.data = feature_id
        
        session['previous'] = prev
        return render_template("modify_review1.html", form = form, numbers_features=numbers_features,
                               numbers_sentiment=numbers_sentiment, suggestions=suggestions)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#PROFIL
@app.route('/changeProfil', methods=['GET','POST'])
@is_logged_in
@user_exist
def change_myProfil():
    try:
        email = session['email']
        login = session['login']

        id = MTA.get_id_by_email(connection=connection, email=email)
        id = id['users_id']
    
        #formUsername = Form.UsernameForm(request.form)
        formPass = Form.ChangePassword(request.form)

        if request.method == 'POST':
            '''
            if request.form['btn'] == 'Save' and formUsername.validate():
                username = formUsername.username.data
                MTA.change_username(connection=connection, id=id, username=username)
                session['login'] = username
                return redirect(url_for('change_myProfil'))
            '''

            if request.form['btn'] == 'Change' and formPass.validate():
                password_candidate = request.form['oldPassword']
                data = MTA.check_pass(connection=connection, login = login, password_candidate = password_candidate)
            
                if data:
                    if sha256_crypt.verify(password_candidate, data['password']):
                        password = request.form['password']
                        password = sha256_crypt.encrypt(str(formPass.password.data))
                        #return jsonify(password)
                        
                        MTA.change_password(connection = connection, login = login, password = password)

                        flash('You changed your password', 'success') 
                        return redirect(url_for('change_myProfil'))
                        
            #return render_template('change_pass.html', form = form)

        #formUsername.username.data = session['login']
        #return 'DOBRE'
        return render_template('myProfil.html', formPass=formPass,
                                 id=id, login=login, email=email)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)
        
    '''
    NEPOTREBNE
    try:
        form = Form.ChangePassword(request.form)
        
        if request.method == 'POST' and form.validate():
            login = session['login']

            password_candidate = request.form['oldPassword']
            data = MTA.get_pass(connection=connection, login = login)
            
            if data :
                data = data['password']
                if sha256_crypt.verify(password_candidate, data):
                    password = sha256_crypt.encrypt(str(form.password.data))
                    #password = request.form['password']
                    MTA.change_password(connection = connection, login = login, password = password)

                    flash('You changed your password', 'success') 

                    return redirect(url_for('change_password'))
                    #return redirect(url_for('get_reviews_features')) 

        return render_template('change_pass.html', form = form)

    except:
        flash('Nieco je zle', 'danger') #prepisat
        return render_template('change_pass.html', form= form)
    '''

#LOGOUT
@app.route('/logout', methods=['GET'])
@is_logged_in
@user_exist
def logout():
    session.clear()
    flash('You are now logged out', 'success')
    return redirect(url_for('login'))

@app.route('/slashboard')
def slashboard():
    try:
        return render_template('slashboard.html', TOPIC_DICT=shamwow)
    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#Evidence
@app.route('/evidence', methods=['GET','POST'])
@is_logged_in
@is_admin
@user_exist
def get_all_evidence():
    try:
        users = MTA.get_users(connection=connection)
        all = {"users_id" : 0, "name" : "ALL"}
        users.insert(0,all)
       
        #nasypanie moznosti do SelectField-u
        groups_list=[(u['users_id'], u['name']) for u in users]

        form = Form.ChooseUser()
        form.attribute.choices = groups_list

        result = MTA.get_evidence_sub_rev(connection=connection)

        a = []

        if request.method =='POST': 
            attribute = request.form['attribute']
            dt1 = request.form['dt1']
            dt2 = request.form['dt2']

            if not dt2 and dt1:
                dt2 = '2099-12-31'

            if not dt1 and dt2:
                dt1 = '2000-01-01'

            if dt2 < dt1 :
                flash('The start day cannot be earlier than the End day', 'danger')
                return redirect(url_for('get_all_evidence'))

            dt2 = dt2 + ' 24:00:00'

            if attribute == '0':
                if (dt1 and dt2):
                    result = MTA.get_evidence_sub_rev_by_date(connection=connection, start_date = dt1, end_date = dt2)
                else:
                    result = MTA.get_evidence_sub_rev(connection=connection)

                if a:
                    result = a
                
                def get_result(offset=0, per_page=10):
                    return result[offset: offset + per_page]

                page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
                total = len(result)
                pagination_users = get_result(offset=offset, per_page=per_page)
            #   return jsonify(pagination_users)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                            css_framework='bootstrap4')

                return render_template("evidence.html",users=pagination_users,
                                        page=page,
                                       per_page=per_page,
                                       pagination=pagination,
                                       form = form)
            
            
            if (dt1 and dt2):
                result = MTA.get_evidence_by_date(connection=connection,id_login=attribute, start_date = dt1, end_date = dt2)
            else:
                result = MTA.get_evidence(connection=connection, id_login=attribute)
            
            if a:
                result = [i for i in a for j in result if i['reviews_suggestions_id']==j['reviews_suggestions_id']] 
            #return jsonify(result)


            def get_result(offset=0, per_page=10):
                return result[offset: offset + per_page]

            page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
            total = len(result)
            pagination_users = get_result(offset=offset, per_page=per_page)
        #   return jsonify(pagination_users)
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4')

            return render_template("evidence.html",users=pagination_users,
                                   page=page,
                                   per_page=per_page,
                                   pagination=pagination,
                                   form = form)
            #return redirect(url_for('get_reviews_suggestions'))

        if not result:
            flash('Evidence is empty', 'danger')
            return render_template("evidence.html", result = result, form = form)
            return error_status(error_msg='Products not found', status_code=404)
        else:
            def get_result(offset=0, per_page=10):
                return result[offset: offset + per_page]

            page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
            total = len(result)
            pagination_users = get_result(offset=offset, per_page=per_page)
        #   return jsonify(pagination_users)
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4')
            return render_template("evidence.html", 
                               users=pagination_users,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form = form)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

'''
@app.route('/evidence', methods=['GET','POST'])
@is_logged_in
@is_admin
def get_all_evidence():
    users = MTA.get_users(connection=connection)
    all = {"users_id" : 0, "name" : "ALL"}
    users.insert(0,all)
   
    #nasypanie moznosti do SelectField-u
    groups_list=[(u['users_id'], u['name']) for u in users]

 #   return jsonify(users)

    form = Form.ChooseUser()
    form.attribute.choices = groups_list

    result = MTA.get_all_evidence(connection=connection)

    def get_result(offset=0, per_page=10):
        return result[offset: offset + per_page]

    page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
    total = len(result)
    pagination_users = get_result(offset=offset, per_page=per_page)
#    return jsonify(pagination_users)
    pagination = Pagination(page=page, per_page=per_page, total=total,
                            css_framework='bootstrap4')

    if request.method =='POST': 
        #vratim len kde je zmenene feature
        if request.form.get('changed-feature'):
            #vratim to kde su zmenene obe
            if request.form.get('changed-sentiment'):
                return 'OBA'
            return 'Feature'
        #vratim kde je zmeneny sentiment
        elif request.form.get('changed-sentiment'):
            a = MTA.get_changed_sentiment(connection=connection)
            return jsonify(a)

        else:
            #vracia id loginu
            attribute = request.form['attribute']
            
            if attribute == '0':
                result = MTA.get_all_evidence(connection=connection)
                def get_result(offset=0, per_page=10):
                    return result[offset: offset + per_page]

                page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
                total = len(result)
                pagination_users = get_result(offset=offset, per_page=per_page)
            #    return jsonify(pagination_users)
                pagination = Pagination(page=page, per_page=per_page, total=total,
                                        css_framework='bootstrap4')

                return render_template("evidence.html",users=pagination_users,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form = form)
            
            result = MTA.get_evidence(connection=connection, id_login=attribute)
            def get_result(offset=0, per_page=10):
                    return result[offset: offset + per_page]

            page, per_page, offset = get_page_args(page_parameter='page',per_page_parameter='per_page')
            total = len(result)
            pagination_users = get_result(offset=offset, per_page=per_page)
            #    return jsonify(pagination_users)
            pagination = Pagination(page=page, per_page=per_page, total=total,
                                    css_framework='bootstrap4')


            return render_template("evidence.html",users=pagination_users,
                               page=page,
                               per_page=per_page,
                               pagination=pagination,
                               form = form)
            #return redirect(url_for('get_reviews_suggestions'))

    if not result:
        return render_template("evidence.html", result = result, form = form)
        #return error_status(error_msg='Products not found', status_code=404)
    else:
        return render_template("evidence.html", 
                           users=pagination_users,
                           page=page,
                           per_page=per_page,
                           pagination=pagination,
                           form = form)
'''

#SUGGESTIONS PREVIEW
@app.route('/suggestions', methods=['GET','POST'])
@is_logged_in
@is_admin
@user_exist
def get_reviews_suggestions1():
    try:
        users = MTA.get_users(connection=connection)
        all = {"users_id" : 0, "name" : "All"}
        users.insert(0,all)
        
        #nasypanie moznosti do SelectField-u
        groups_list=[(u['users_id'], u['name']) for u in users]

        result = MTA.get_suggestions(connection=connection)
        suggestions = MTA.get_count_of_suggestions(connection=connection)
        #return jsonify(suggestions)

        if request.method =='POST': 
            try:
                attribute = request.form['attribute']
                start_date = request.form['dt1']
                end_date = request.form['dt2']

                if not start_date:
                    start_date = '2000-01-01'
                if not end_date:
                    end_date = '2099-12-31'

                end_date = end_date + ' 24:00:00'

                if end_date < start_date :
                    flash('The start day cannot be earlier than the End day', 'danger')
                    return redirect(url_for('get_reviews_suggestions1'))

                form = Form.SearchForm3(attribute=attribute)
                form.attribute.choices = groups_list
                search = request.form['search']
                state = request.form['state']

                if state == 'czech_name':
                    table = 'product_categories'
                elif state == 'text_review':
                    table = 'reviews'
                    filter = 'text'
                    state = 'recenzia'
                elif state == 'zmena_fea':
                    table = 'feature_names'
                    filter = 'text'
                else:
                    table = 'reviews'

                if attribute == '0':
                    if search:
                        if state == "reviews_id" or state == "sentiment" or state == "czech_name":
                            if state == "reviews_id":
                                if not (search.isnumeric()):
                                    flash('Id number cant be string', 'danger')
                                    return redirect(url_for('get_reviews_suggestions1'))
                            
                            filter = state
                        elif state == "zmena_sen":
                            table = "r1"
                            filter = "sentiment"
                        elif state == "text":
                            table = "r2"
                            filter = "feature"



                        result = MTA.example4(connection = connection, start_date = start_date, end_date = end_date)

                        if filter == 'text':
                            if table == 'reviews':
                                value = '%'+search+'%'
                            else:
                                value = search
                            suggestions = MTA.get_count_of_suggestions_by_date_filter_ilike(connection = connection, start_date = start_date, end_date =end_date,
                                                                                  table = table, filter = filter, value = value)
                            #return jsonify(suggestions)
                        elif filter == "feature":
                            suggestions = MTA.get_count_of_suggestions_by_date_filter1(connection = connection, start_date = start_date, end_date =end_date,
                                                                                  table = table, filter = filter, value = search)
                        else:
                            suggestions = MTA.get_count_of_suggestions_by_date_filter(connection = connection, start_date = start_date, end_date =end_date,
                                                                                  table = table, filter = filter, value = search)
                        #return jsonify(suggestions)
                        asd = []
                        #return jsonify(result)
                        for r in result:
                            #return jsonify(r)
                            if str(state) == 'reviews_id':
                                if search == str(r[state]):
                                    asd.append(r)
                            else:
                                if search in r[state]:
                                    asd.append(r)
                        result = asd  
                        #return jsonify(suggestions)
                        #result = MTA.example(connection=connection, filter = state, value=search, table = table, start_date = start_date, end_date = end_date) 
                        #return jsonify(start_date)
                    else:
                        suggestions = MTA.get_count_of_suggestions_by_date(connection=connection, start_date = start_date, end_date = end_date)
                        result = MTA.get_suggestions_by_date(connection=connection, start_date = start_date, end_date = end_date)

                    return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)
                else:
                    suggestions = MTA.get_count_of_suggestions_user(connection = connection, users_id = attribute)

                    if search:
                        #result = MTA.example2(connection = connection, login = attribute, filter = state, value=search, table = table, start_date = start_date, end_date = end_date)
                        result = MTA.example3(connection = connection, login = attribute, start_date = start_date, end_date = end_date)
                        asd = []
                        #return jsonify(result)
                        for r in result:
                            #return jsonify(r)
                            if str(state) == 'reviews_id':
                                if search == str(r[state]):
                                    asd.append(r)
                            else:
                                if search in r[state]:
                                    asd.append(r)
                        result = asd  
                        #return jsonify(result)
                    else:
                        result = MTA.get_suggestion_by_user(connection = connection, login = attribute, start_date = start_date, end_date = end_date)       
                    return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)        
            except NameError as e:
                logging.warning('%s \n',[e])
                return error_status(error_msg=e)
                flash('Something is going wrong' + e, 'danger')
                prev = request.referrer
                return redirect(prev, code=302)
                    
        form = Form.SearchForm3(meta={'locales': ['en_US', 'en']})
        form.attribute.choices = groups_list
       
        if not result:
            return render_template("suggestions1.html", result = result, form = form)
            #return error_status(error_msg='Products not found', status_code=404)
        else:
            return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)
        
    except NameError as e:
        logging.warning('%s \n',[e])
        return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#SUGGESTION RETURN
@app.route('/suggestion/<int:id>', methods=['GET', 'POST'])
@is_logged_in
@is_admin
@user_exist
def reject_suggestion(id):
    try:
        users_id = MTA.get_id_by_email(connection=connection, email=session['email'])
        users_id = users_id['users_id']
        result = MTA.get_suggestion_byID(connection=connection, id=id)
        #return jsonify(result)
      
        MTA.return_suggestion(connection = connection, users_id = result['users_id'], reviews_id=result['reviews_id'],
                              sentiment=result['sentiment'], attribute=result['feature_names_id'])
        
        flash('Suggestion rejected', 'success')
        prev = request.referrer
        return redirect(prev, code=302)
        return redirect(url_for('get_reviews_suggestions'))

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#SUGGESTION MODIFY
@app.route('/suggestions/modify/<int:id>', methods=['GET', 'POST'])
@is_logged_in
@is_admin
@user_exist
def modify_suggestions(id):
    try:
        result = MTA.get_review(connection=connection, id=id)
        resp = dict(result)

        product_categories_id = MTA.get_category_id(connection=connection, category=resp['czech_name'])['product_categories_id']
        
        feature = resp['feature']
        language = resp['languages_id']

        numbers_features = MTA.get_sug_fea_for_review(connection=connection, reviews_id = id)
        numbers_sentiment = MTA.get_sug_sent_for_review(connection=connection, reviews_id = id)
        
        #natiahnutie idciek a textu 
        features_text = MTA.get_features_by_language(connection=connection, product_categories_id=product_categories_id, languages_id = language)

        #vytiahnutie navrhov pre danu recenziu
        sugg_reviews= MTA.get_suggestions_for_review(connection=connection, reviews_id = id)

        #nasypanie moznosti do SelectField-u
        groups_list=[(f['features_id'], f['text']) for f in features_text]

        languages = MTA.get_languages(connection=connection)
        #nasypanie moznosti do SelectField-u
        groups_list1=[(l['languages_id'], l['title_english']) for l in languages]

        form = Form.ReviewForm()
        for x in features_text:
                if x['text'] == feature:
                    features_id = x['features_id'] 
                    feature_names_id = x['feature_names_id']

        #aa = MTA.get_language_by_feature(connection=connection, features_id= feature_names_id)['languages_id']
        
        form.review_text.data = resp['text']
        form.category.data = resp['czech_name']
        form.sentiment.data = resp['sentiment']
        form.attribute.choices = groups_list
        form.language.choices = groups_list1

        if request.method =='POST':
            if request.form['action'] == 'Submit & Accept':
                sentiment = request.form['sentiment']
                attribute = request.form['attribute']
                language = request.form['language']

                features_names_id = MTA.get_features_by_features_id_lang_product_category(connection=connection, features_id = attribute, languages_id = language,
                                                                             product_categories_id = product_categories_id)['feature_names_id']

                same_suggestions= MTA.get_correct_suggestions(connection=connection, reviews_id = id,
                                                            features_id=features_names_id, sentiment=sentiment)

                rest_suggestions = MTA.get_incorrect_suggestions(connection=connection, reviews_id=id,
                                                                features_id=features_names_id, sentiment=sentiment)

                if rest_suggestions:
                    for sug in rest_suggestions:
                        MTA.reject_suggestion(connection=connection,users_id = sug['users_id'],
                                              reviews_id= id, sentiment = sug['sentiment'],
                                              attribute= sug['feature_names_id'])

                if same_suggestions:
                    for sug in same_suggestions:
                        MTA.accept_suggestion(connection=connection, user_id=sug['users_id'], review_id= id,
                                              sentiment = sug['sentiment'],
                                              attribute= sug['feature_names_id'])
                else:
                    users_id = MTA.get_id_by_email(connection=connection, email=session['email'])
                    MTA.accept_suggestion(connection=connection, user_id=users_id['users_id'], review_id=id,
                                         sentiment= sentiment, attribute=features_names_id)
                
                MTA.change_review(connection = connection, feature_id = features_names_id, sentiment=sentiment, review_id=id)
                
                flash('Suggestion Updated & Accepted', 'success')
                return redirect(url_for('get_reviews_suggestions1')) 
           
        form.language.data = language
        form.attribute.data = features_id
        

        return render_template("modify_suggestions.html", form = form, numbers_sentiment=numbers_sentiment,
                                numbers_features=numbers_features, sugg_reviews=sugg_reviews)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#SUGGESTIONS MODIFY
@app.route('/suggestion/modify/<int:id>', methods=['GET', 'POST'])
@is_logged_in
@is_admin
@user_exist
def modify_suggestion(id):
    try:
        #natiahnutie review
        result = MTA.get_review_for_edit_ext(connection=connection, reviews_suggestions_id=id)
        resp = dict(result)
        
        feature = resp['feature']
        language = resp['languages_id']
        review_id = resp['reviews_id']

        product_categories_id = MTA.get_category_id(connection=connection, category=resp['czech_name'])['product_categories_id']

        #numbers_features = MTA.get_sug_fea_for_review(connection=connection, reviews_id = review_id)
        #numbers_sentiment = MTA.get_sug_sent_for_review(connection=connection, reviews_id = review_id)
        #natiahnutie idciek a textu 
        features_text = MTA.get_features_by_language(connection=connection, product_categories_id=product_categories_id, languages_id = language)
        #features_text = MTA.get_features_text(connection=connection,
        #product_categories_id=product_categories_id['product_categories_id'])

        #vytiahnutie navrhov pre danu recenziu
        #sugg_review= MTA.get_suggestions_for_review(connection=connection, reviews_id = review_id)
        #nasypanie moznosti do SelectField-u
        groups_list=[(f['features_id'], f['text']) for f in features_text]

        languages = MTA.get_languages(connection=connection)
        #nasypanie moznosti do SelectField-u
        groups_list1=[(l['languages_id'], l['title_english']) for l in languages]

        form = Form.ReviewForm()
        for x in features_text:
                if x['text'] == feature:
                    features_id = x['features_id']
                    feature_names_id = x['feature_names_id']

        #return jsonify(feature_names_id)

        #aa = MTA.get_language_by_feature(connection=connection, features_id= fature_names_id)['languages_id']#resp['reviews_id'])['languages_id']

        form.review_text.data = resp['text']
        form.category.data = resp['czech_name']
        form.sentiment.data = resp['sentiment']
        form.attribute.choices = groups_list
        form.language.choices = groups_list1

        if request.method =='POST':
            if 'attribute' in request.form:
                if request.form['action'] == 'Submit & Accept':
                    ## TODO
                    sentiment = request.form['sentiment']
                    attribute = request.form['attribute']
                    language = request.form['language']
                    
                    features_names_id = MTA.get_features_by_features_id_lang_product_category(connection=connection, features_id = attribute, languages_id = language,
                                                                             product_categories_id = product_categories_id)['feature_names_id']
                    
                    sug = MTA.get_suggestion_byID(connection = connection, id = id)
                    #return jsonify(sug)

                    same_suggestions= MTA.get_correct_suggestions(connection=connection, reviews_id = sug['reviews_id'],
                                                                features_id=features_names_id, sentiment=sentiment)

                    rest_suggestions = MTA.get_incorrect_suggestions(connection=connection, reviews_id=sug['reviews_id'],
                                                                    features_id=features_names_id, sentiment=sentiment)

                    #return jsonify(rest_suggestions)
                    if rest_suggestions:
                        for sug in rest_suggestions:
                            MTA.reject_suggestion(connection=connection,users_id = sug['users_id'],
                                                  reviews_id= sug['reviews_id'], sentiment = sug['sentiment'],
                                                  attribute= sug['feature_names_id'])

                    if same_suggestions:
                        for sug in same_suggestions:
                            MTA.accept_suggestion(connection=connection, user_id=sug['users_id'], review_id= sug['reviews_id'],
                                                  sentiment = sug['sentiment'],
                                                  attribute= sug['feature_names_id'])
                    else:
                        users_id = MTA.get_id_by_email(connection=connection, email=session['email'])
                        MTA.accept_suggestion(connection=connection, user_id=users_id['users_id'], review_id=sug['reviews_id'],
                                             sentiment= sentiment, attribute=features_names_id)
                    
                    MTA.change_review(connection = connection, feature_id = features_names_id, sentiment=sentiment, review_id=sug['reviews_id'])
                    
                    flash('Suggestion Updated & Accepted', 'success')
                    return redirect(url_for('get_reviews_suggestions')) 
            else:
                flash('Something is going wrong', 'danger')
                prev = request.referrer
                return redirect(prev, code=302)
        


        form.attribute.data = features_id
        form.language.data = language

        return render_template("modify_suggestion.html", form = form)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)

#SUGMIT SINGLE SUGGESTION
@app.route('/suggestion/submit/<string:id>', methods=['GET'])
@is_logged_in
@is_admin
@user_exist
def submit_suggestion(id):
    try:
        #vytiahnutie info z navrhu
        sug = MTA.get_suggestion_byID(connection = connection, id = id)
        user_id = MTA.get_id_by_email(connection=connection,email=session['email'])
        user_id = user_id['users_id']

        #potvrdenie a vymazanie navrhu
        MTA.change_review(connection = connection, feature_id = sug['feature_names_id'],
                          sentiment=sug['sentiment'], review_id=sug['reviews_id'])

        #MTA.accept_suggestion(connection=connection, user_id=sug['users_id'], review_id=sug['reviews_id'],
        #                     sentiment=sug['sentiment'], attribute = sug['feature_names_id'])    

        same_suggestions= MTA.get_correct_suggestions(connection=connection, reviews_id = sug['reviews_id'],
                                                            features_id=sug['feature_names_id'], sentiment=sug['sentiment'])

                
        rest_suggestions = MTA.get_incorrect_suggestions(connection=connection, reviews_id=sug['reviews_id'],
                                                                features_id=sug['feature_names_id'], sentiment=sug['sentiment'])
        #return jsonify(rest_suggestions)

        if rest_suggestions:
            for sug in rest_suggestions:
                MTA.reject_suggestion(connection=connection,users_id = sug['users_id'],
                                      reviews_id= sug['reviews_id'], sentiment = sug['sentiment'],
                                      attribute= sug['feature_names_id'])

        if same_suggestions:
            for sug in same_suggestions:
                MTA.accept_suggestion(connection=connection, user_id=sug['users_id'], review_id= sug['reviews_id'],
                                      sentiment = sug['sentiment'],
                                      attribute= sug['feature_names_id'])


        #MTA.delete_suggestion(connection = connection, id = id)

        flash('Review was edited ', 'success')
        #prev = request.referrer
        #return redirect(prev, code=302)
        return redirect(url_for('get_reviews_suggestions'), code=302)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)
    
#rACCOUNTS
# TODO
#pri zlyhani to nerefreshne stranku
@app.route('/register', methods=['GET','POST'])
@is_logged_in
@is_admin
@user_exist
def register():
    try:
        role = MTA.get_user_role(connection=connection, login= session['login'])
        result = MTA.get_users(connection=connection)
        roles = MTA.get_roles(connection=connection)

        #nasypanie moznosti do SelectField-u
        groups_list=[(role['user_roles_id'], role['name']) for role in roles]

        
        form = Form.RegisterForm(request.form)
        form.role.choices = groups_list

        if request.method == 'POST' and form.validate():
            username = form.username.data
            email = form.email.data
            password = sha256_crypt.encrypt(str(form.password.data))
            role = form.role.data
            
            is_valid = check_validity_of_email(email)
            if is_valid:
                user_added= MTA.add_user(connection=connection, username=username, password=password, role=role, email=email)
                
                if user_added:
                    MTA.add_user(connection=connection, username=username, password=password, role=role, email=email)
                    flash('You registrated a new member', 'success') 
                    return redirect(url_for('register')) 

                else:
                    error = "Email or Username is already registered"
                    return render_template('register.html', form = form, result = result, role = role, error = error)
            else:
                error = "Email is not valid!"
                return render_template('register.html', form = form, result = result, role = role, error = error)

        return render_template('register.html', form = form, result = result, role = role)
    
    except (RuntimeError, TypeError, NameError, Exception) as e:
        logging.warning('%s \n',[e])
        flash('Something is going wrong', 'danger')
        return render_template('register.html', form = form, result = result, role = role)

#DELETE USER
@app.route('/deleteUser/<int:id>', methods=['POST'])
@is_logged_in
@is_admin
@user_exist
def delete_user(id):
    MTA.delete_user(connection = connection, id = id)
    return redirect(url_for('register')) 


#EDIT OTHER USERS
@app.route('/changeProfil/<int:id>', methods=['GET','POST'])
@is_logged_in
@is_admin
@user_exist
def change_profil(id):
    roles = MTA.get_roles(connection=connection)

    #nasypanie moznosti do SelectField-u
    groups_list=[(role['user_roles_id'], role['name']) for role in roles]

    login = MTA.get_login_byID(connection=connection, id=id)
    login = login['name']
    role = MTA.get_role_by_id(connection=connection, id=id)
    role = role['name']
    email = MTA.get_email_by_login(connection=connection, login=login)
    email = email['email']
    
    try:
        formRole = Form.RoleForm(request.form)
        #formUsername = Form.UsernameForm(request.form)
        formPass = Form.ChangePasswordToAnotherUser(request.form)

        formRole.role.choices = groups_list
        #return jsonify(groups_list)
        for x in roles:
            if x['name'] == role:
                a = x['user_roles_id']

        if request.method == 'POST':
            #TODO nevracia to spravne meno
            '''
            if request.form['btn'] == 'Save' and formUsername.validate():
                username = formUsername.username.data
                MTA.change_username(connection=connection, id=id, username=username)
                if login == session['login']:
                    session['login'] = username
                return redirect(url_for('register', id=id))
            '''
            if request.form['btn'] == 'Change' and formRole.validate():
                role = formRole.role.data
                #return jsonify(role)
                MTA.change_user_data(connection=connection, id = id, role = role)
                if login == session['login']:
                    if role == 1:
                        session.pop('is_admin')
                    elif role == 2:
                        session['is_admin'] = True
                flash('Role was succesfully changed', 'success')
                return redirect(url_for('register')) 

            elif request.form['btn'] == 'Change Password' and formPass.validate():
                try:
                    login = MTA.get_login_byID(connection = connection, id= id)
                    login = login['name']     
                    
                    password = request.form['password']
                    password = sha256_crypt.encrypt(str(password))
                    
                    MTA.change_password(connection = connection, login = login, password = password)

                    flash('You changed your password', 'success') 
                    return redirect(url_for('change_profil', id=id))
                except:
                    flash('Nieco je zle', 'danger') #prepisat
                    return redirect(url_for('change_profil', id=id))
                
        name = MTA.get_login_byID(connection=connection, id=id) 
        name = name['name']   

        formRole.role.data = a
        #formUsername.username.data = name
            
        return render_template('profil.html', formRole=formRole,
                               formPass=formPass, id=id, login=login, email=email)

    except NameError as e:
        logging.warning('%s',[e])
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)
        
'''
#FORM na zmenu role a mena spolu
#zmena role TREBA ZMENIT METODU
@app.route('/changeProfil/<int:id>', methods=['GET','POST'])
@is_logged_in
@is_admin
def change_profil(id):
    roles = MTA.get_roles(connection=connection)

    #nasypanie moznosti do SelectField-u
    groups_list=[(role['user_roles_id'], role['name']) for role in roles]

    try:
        form = Form.UserForm(request.form)
        form.username.data = session['login']
        form.role.choices = groups_list

        #return 'ahoj'

        if request.method == 'POST' and form.validate():
            role = form.role.data
            username = form.username.data
            
            MTA.change_user_data(connection=connection, id = id, role = role, username=username)
            flash('Role was succesfully changed', 'success')
            return redirect(url_for('register')) 
        return render_template('change_role.html', form = form)

    except:
        flash('Something is going wrong', 'danger')
        return render_template('register.html', form= form)
'''    
    


    


#uvodna stranka po prihlaseni   
@app.route('/hello', methods=['GET'])
@is_logged_in 
def hello():
    #return sys.version 
    return render_template('hello.html')

'''
NEPOUZIVAM
#zmena hesla vybraneho uzivatela
@app.route('/changePassword/<int:id>', methods=['GET','POST'])
@is_logged_in
def change_password1(id):
    try:
        form = Form.ChangePassword(request.form)
        if request.method == 'POST' and form.validate():
            #vytiahnutie loginu podla id uzivatela
            login = MTA.get_login_byID(connection = connection, id= id)
            login = login['name']     
            password_candidate = request.form['oldPassword']

            data = MTA.check_pass(connection=connection, login = login, password_candidate = password_candidate)
            
            if data :
                if sha256_crypt.verify(password_candidate, data['password']):
                    
                    password = request.form['password']
                    password = sha256_crypt.encrypt(str(form.password.data))
                    #return jsonify(password)
                    
                    MTA.change_password(connection = connection, login = login, password = password)

                    flash('You changed your password', 'success') 
                    return redirect(url_for('change_password1'))
                    #return redirect(url_for('get_reviews_features')) 

        return render_template('change_pass.html', form = form)
    except:
        flash('Nieco je zle', 'danger') #prepisat
        return render_template('change_pass.html', form= form)
'''



@app.route("/livesearch",methods=["POST","GET"])
def livesearch():
    searchbox = request.form.get("text")
    #filter = request.form.get("filter")
    filter = 'reviews_id'
    email = session['email']
    id = MTA.get_id_by_email(connection=connection, email=email)
    #return jsonify(filter)
    #TODO#
    #Dokoncit podmienky if
    if filter == 'reviews_id':
        result = MTA.get_reviews_features_filtration_reviews_id(connection=connection, id = id['users_id'],search = searchbox)
        return jsonify(result)
    elif filter == 'features_names_id':
        result = MTA.get_reviews_features_filtration_feature(connection=connection, id = id['users_id'], search = searchbox)
        return jsonify(result)
    else:
        result = MTA.get_reviews_features_filtration_sentiment(connection=connection, id = id['users_id'], search = searchbox)
        return jsonify(result)

@app.route('/review/state/<get_state>/<get_category>')
def statebycountry(get_state, get_category):
    if get_state == 'sentiment':
        stateArray = [{'positive': 'positive'},{'negative': 'negative'}, {'neutral': 'neutral'}, {'unspecified': 'unspecified'}]
        return jsonify({'statecountry' : stateArray})
    else:
        category_id = MTA.get_category_id(connection=connection, category=get_category)['product_categories_id']
        features_text = MTA.get_lang_features_text(connection=connection, languages_id = get_state, category_id = category_id)
        stateArray = []
        for f in features_text:
            stateObj = {}
            #stateObj['id'] = f['feature_names_id']
            stateObj['id'] = f['features_id']
            stateObj['name'] = f['text']
            stateArray.append(stateObj)
        return jsonify({'statecountry' : stateArray})

@app.route('/review/return/state/<get_state>/<get_category>')
def features_fetch(get_state,get_category):
    if get_state == 'sentiment':
        stateArray = [{'positive': 'positive'},{'negative': 'negative'}, {'neutral': 'neutral'}, {'unspecified': 'unspecified'}]
        return jsonify({'statecountry' : stateArray})
    else:
        category_id = MTA.get_category_id(connection=connection, category=get_category)['product_categories_id']
        features_text = MTA.get_lang_features_text(connection=connection, languages_id = get_state, category_id = category_id)
        stateArray = []
        for f in features_text:
            stateObj = {}
            #stateObj['id'] = f['feature_names_id']
            stateObj['id'] = f['features_id']
            stateObj['name'] = f['text']
            stateArray.append(stateObj)
        return jsonify({'statecountry' : stateArray})




    
@app.route('/review/return/<int:id>', methods=['GET','POST'])
@is_logged_in
@user_exist
def returned_review(id):
    try:
        id_users = MTA.get_id_by_email(connection=connection, email = session['email'])['users_id']
        result = MTA.get_review_ext(connection=connection, id=id)
        #result = MTA.get_returned_reviews_for_modify(connection=connection, id_users = id_users, reviews_id = id)
        #return jsonify(result)
        #natiahnutie review
        prev = request.referrer
        
        product_categories_id = MTA.get_category_id(connection=connection, category=result['czech_name'])
        product_categories_id = product_categories_id['product_categories_id']

        feature = result['text']
        
        language = result['languages_id']
        
        #return jsonify(language)
        numbers_features = MTA.get_sug_fea_for_review(connection=connection, reviews_id = result['reviews_id'])
        numbers_sentiment = MTA.get_sug_sent_for_review(connection=connection, reviews_id = result['reviews_id'])

        suggestions = MTA.get_suggestions_for_review(connection=connection, reviews_id = result['reviews_id'])
        returned_suggestions = MTA.get_returned_suggestions_for_review(connection=connection, reviews_id = result['reviews_id'])
        #return jsonify(returned_suggestions)
        #natiahnutie idciek a textu 
        #languages = MTA.get_language_by_feature(connection=connection, features_id= resp['reviews_id'])
        languages = MTA.get_languages(connection=connection)
        #nasypanie moznosti do SelectField-u
        groups_list1=[(l['languages_id'], l['title_english']) for l in languages]

        #natiahnutie idciek a textu 
        #return jsonify(language)
        features_text = MTA.get_features_by_language(connection=connection, product_categories_id=product_categories_id, languages_id = language)
        #nasypanie moznosti do SelectField-u
        #groups_list=[(f['feature_names_id'], f['text']) for f in features_text]
        groups_list=[(f['features_id'], f['text']) for f in features_text]


        form = Form.ReviewForm()

        for x in features_text:
            if x['text'] == feature:
                #a = x['feature_names_id']
                a = x['features_id']
                b = x['feature_names_id']
        aa = MTA.get_language_by_feature(connection=connection, features_id= b)['languages_id']#resp['reviews_id'])['languages_id']
        
        form.review_text.data = result['popis']
        form.language.choices = groups_list1
        form.category.data = result['czech_name']
        form.sentiment.data = result['sentiment']
        form.attribute.choices = groups_list

        if request.method =='POST': 
            #return jsonify('idem')
            if a == int(request.form['attribute']) and result['sentiment'] == request.form['sentiment'] and int(request.form['language']) == result['languages_id']:
                flash('No change has been designed', 'danger')
                return render_template("modify_review1.html", form = form, numbers_features=numbers_features,
                               numbers_sentiment=numbers_sentiment, suggestions=suggestions, returned_suggestions=returned_suggestions)
                return 'Nebola navrhnuta ziadna zmena'
            prev = session['previous']
            
            review_text = result['text']
            form.category.data = result['czech_name']
            sentiment = request.form['sentiment']
            attribute = request.form['attribute']
            language = request.form['language']
            email = session['email']

            features_names_id = MTA.get_feature_by_lang_fea_product_cat(connection=connection, features_id = attribute, languages_id = language, product_categories_id=product_categories_id)
            #features_names_id = features_names_id['feature_names_id']
            #return jsonify(features_names_id)

            id_login = MTA.get_id_by_email(connection=connection, email=email)
            id_login = id_login['users_id']

            MTA.add_suggestion(connection=connection, id_login=id_login, id=id, sentiment=sentiment, attribute=features_names_id['feature_names_id'])

            flash('Review Updated', 'success')
            return redirect(prev, code=302)
            #return redirect(url_for('get_reviews_features'))

        form.language.data = aa
        form.attribute.data = a
        
        session['previous'] = prev
        return render_template("modify_review1.html", form = form, numbers_features=numbers_features,
                               numbers_sentiment=numbers_sentiment, suggestions=suggestions, returned_suggestions=returned_suggestions)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)




#vypis navrhov na zmenu
@app.route('/suggestionss', methods=['GET','POST'])
@is_logged_in
@is_admin
@user_exist
def get_reviews_suggestions():
    try:
        users = MTA.get_users(connection=connection)
        all = {"users_id" : 0, "name" : "All"}
        users.insert(0,all)
        
        #nasypanie moznosti do SelectField-u
        groups_list=[(u['users_id'], u['name']) for u in users]

        attribute = request.args.get('attribute')
        start_date = request.args.get('dt1')
        end_date = request.args.get('dt2')
        result = MTA.get_suggestions(connection=connection)
        suggestions = MTA.get_count_of_suggestions(connection=connection)

        if attribute:
            form = Form.SearchForm3(attribute=attribute)
            form.attribute.choices = groups_list
            search = request.args.get('search')
            state = request.args.get('state')

            if state == 'czech_name':
                table = 'product_categories'
            else:
                table = 'reviews'

            if attribute == '0':
                if search:
                    #return jsonify(search)
                    result = MTA.example(connection=connection, filter = state, value=search, table = table) 
                    #return jsonify(result)
                else:
                    #return jsonify(search)
                    result = MTA.get_suggestions(connection=connection)

                return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)
            else:
                if search:
                    result = MTA.example2(connection = connection, login = attribute, filter = state, value=search, table = table)
                else:
                    result = MTA.get_suggestion_by_user(connection = connection, login = attribute)       
                return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)

        else:
            form = Form.SearchForm3()
            form.attribute.choices = groups_list
        
            if not result:
                return render_template("suggestions1.html", result = result, form = form)
                #return error_status(error_msg='Products not found', status_code=404)
            else:
                return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)


        '''
        if attribute:
            form = Form.ChooseUser(attribute=attribute)
            form.attribute.choices = groups_list

            if attribute == '0':
                result = MTA.get_suggestions(connection=connection)
                #return jsonify(result)
                return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)

            result = MTA.get_suggestion_by_user(connection = connection, login = attribute)       
            return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)

        else:
            form = Form.ChooseUser()
            form.attribute.choices = groups_list
        
            if not result:
                return render_template("suggestions1.html", result = result, form = form)
                #return error_status(error_msg='Products not found', status_code=404)
            else:
                return render_template("suggestions1.html", result = result, form = form, suggestions=suggestions)
        '''
        
    except NameError as e:
        logging.warning('%s \n',[e])
        return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)



#TODO prerobit, funkcia je rovnaka ako v edite review
#TODO po edite nakolko pridam aj navrh aj potvrdenie control, tak mi to zobrazi 2x
#moznosti riesenia zobrazovanie len jedneho, 
@app.route('/mojeEvidence/<int:id>', methods=['GET', 'POST'])
@is_logged_in
@user_exist
def review1(id):
    try:
        #natiahnutie review
        result = MTA.get_review_for_edit(connection=connection, reviews_suggestions_id=id)
        resp = dict(result)
        feature = resp['sug_fea']
        
        product_categories_id = MTA.get_category_id(connection=connection, category=resp['czech_name'])
        product_categories_id = product_categories_id['product_categories_id']

        #natiahnutie idciek a textu 
        features_text = MTA.get_features_text(connection=connection, product_categories_id=product_categories_id)

        #nasypanie moznosti do SelectField-u
        groups_list=[(f['feature_names_id'], f['text']) for f in features_text]

        form = Form.ReviewForm()

        for x in features_text:
                if x['text'] == feature:
                    a = x['feature_names_id']


        form.review_text.data = resp['text']
        form.category.data = resp['czech_name']
        form.sentiment.data = resp['sentiment']
        form.attribute.choices = groups_list

        if request.method =='POST': 
            review_text = resp['text']
            sentiment = request.form['sentiment']
            attribute = request.form['attribute']
            login = session['login']

            MTA.change_edited_review(connection=connection, feature_names_id=attribute, sentiment=sentiment, reviews_suggestions_id=id)

            flash('Review Updated', 'success')
            return redirect(url_for('get_evidence')) 

        form.attribute.data= a
        return render_template("modify_review.html", form = form)

    except NameError as e:
        logging.warning('%s \n',[e])
        #return error_status(error_msg=e)
        flash('Something is going wrong', 'danger')
        prev = request.referrer
        return redirect(prev, code=302)


class InfoForm(FlaskForm):
    #startdate = DateField('Start Date', format='%Y-%m-%d', validators=(validators.DataRequired(),))
    #enddate = DateField('End Date', format='%Y-%m-%d', validators=(validators.DataRequired(),))
    submit = SubmitField('Submit')

@app.route('/datum', methods=['GET','POST'])
#@is_logged_in
def picker(id=".datepicker", # identifier will be passed to Jquery to select element
                  dateFormat='yy-mm-dd', # can't be explained more !
                  maxDate='2018-12-30', # maximum date to select from. Make sure to follow the same format yy-mm-dd
                  minDate='2017-12-01',
                  btnsId='.btnId' # id assigned to instigating buttons if needed
                  ): # minimum date
    return render_template("datatime.jinja")





@app.route('/datumm', methods=['POST','GET'])
def hello_world():
    form = Form.ExampleForm()
    if form.validate_on_submit():
        return form.dt.data.strftime('%Y-%m-%d')
    return render_template('datum.html', form=form)

'''
@app.route('/datumm', methods=['GET','POST'])
def indexx():
    form = InfoForm()
    if form.validate_on_submit():
        return jsonify(form.startdate.data)
        return redirect('date')
    return render_template('datum.html', form=form)
'''




if __name__ == '__main__':
    app.secret_key='secret123'
    app.run(host="0.0.0.0", debug=True, port=5000)