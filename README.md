# MTA-Docker

## Návod na spustenie Flask aplikácie v docker:
V root adresári projektu sa nachádza Dockerfile, ktorý stiahne image Ubuntu a nainštaluje potrebné knižnice. Súbor docker-compose.yml zase stiahne image Postgres a Adminer. Spustením príkazu

```sh 
$ sudo docker-compose up 
```
vytvoríme a nalinkujeme vytvorené kontajnery. Rozhranie Adminer je potom dostupné na `localhost:5080`, Postgres beží na porte `:5090`. Aplikácia backendu  funguje na porte `:5000` a implementuje REST API vo frameworku Flask. Dokumentáciu k API nájdete na https://docs.google.com/document/d/1BbyIbKYZL8AGa5gL-ltk-w6rR5rUrsrNr6EbmGldwIQ/edit?usp=sharing, (Jirka už pracuje na peknom Swagger rozhraní)

Prihlásenie do Admineru: 
* server: postgres
* username: postgres
* password: docker
* database: mta


## Štruktúra DB a import dát:
Po prvom spustení je databáza prázdna. Ak chceme vytvoriť tabuľky a naplniť ich dátami, máme na to prirpavené import skripty. V adresári vlezieme do bežiaceho konataineru postgres:

```sh 
$ sudo docker-compose exec postgres /bin/bash
$ sh /tmp/db/import.sh
$ exit
```

Nebo jen `docker-compose exec postgres sh /tmp/db/import.sh`.

Kontajnery si pamätajú data aj po ukončení, nie je preto potrebné vyššie napisaný postup aplikovať pri každom spustení. Vymazať kontajner je možné príkazom 

```sh 
$ sudo docker-compose rm [container name]
```

## Globálna konfigurácia systému:
Konfigurácia je združovaná v module `settings.py`, ktorý ale nie je verzovaný. Je potrebné skopírovať a premenovať súbor `settings.py.example` a nastaviť správne prihlasovacie údaje:

```
DB_HOST = 'postgres'
DB_USERNAME = 'postgres'
DB_PASSWD = 'docker'
DB_NAME = 'mta'
DB_PORT = '5432'
```