from flask_wtf import FlaskForm
from wtforms import Form, StringField, TextAreaField, PasswordField, validators, BooleanField, SelectField,SubmitField, DateField
#from wtforms.fields.html5 import DateField


class RegisterForm(Form):   
    username = StringField('Username', [validators.Length(min=1, max=50)])
    email = StringField('Email', [validators.Length(min=1, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message= 'Password do not match')
        ])
    confirm = PasswordField('Confirm password')
    role = SelectField('Role', coerce=int) 

class ReviewForm(Form):
    review_text = StringField(u'Review')
    language = SelectField('Language', coerce=int)
    category = StringField(u'Category')
    sentiment = SelectField(u'Sentiment', choices=[('positive', 'positive'), ('negative', 'negative'), ('neutral', 'neutral'), ('unspecified', 'unspecified')])
    attribute = SelectField('Feature', coerce=int) 

class ChangePassword(Form):
	oldPassword = PasswordField('Old Password')
	password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message= 'Password do not match')
        ])
	confirm = PasswordField('Confirm password')

class ChangePasswordToAnotherUser(Form):
    password = PasswordField('New Password', [
        validators.DataRequired(),
        ])
   
class ChooseUser(FlaskForm):
    attribute = SelectField('User', coerce=int) 
    dt1 = DateField('Start Date', format='%Y-%m-%d')
    dt2 = DateField('End Date', format='%Y-%m-%d')

class RoleForm(Form):
   role = SelectField('Role', coerce=int) 
   submit1 = SubmitField('Submit')

class UsernameForm(Form):
   username = StringField(u'Username', [validators.Length(min=1, max=50)])
   submit2 = SubmitField('Submit2')

class SearchForm(Form):
    search = SelectField('search', choices=[('', ''),('feature', 'feature'), ('sentiment', 'sentiment')])
    state = SelectField('state', choices=[])

class SearchForm2(Form):
    search = StringField(u'search', [validators.Length(min=1, max=50)])
    state = SelectField(u'state', choices=[('reviews_id', 'id'), ('popis', 'review'), ('text', 'feature'), ('sentiment', 'sentiment'), ('czech_name', 'category')],default='reviews_id')

class SearchForm3(FlaskForm):
    search = StringField(u'search', [validators.Length(min=1, max=50)])
    state = SelectField(u'state', choices=[('reviews_id', 'id'), ('text_review', 'review'), ('text', 'feature'), ('zmena_fea', 'suggest feature'), ('sentiment', 'sentiment'), ('zmena_sen', 'suggest sentiment'), ('czech_name', 'category')],default='reviews_id')
    attribute = SelectField('User', coerce=int)
    dt1 = DateField('Start Date', format='%Y-%m-%d')
    dt2 = DateField('End Date', format='%Y-%m-%d')

class MyBaseForm(FlaskForm):
    class Meta:
        locales = ['en']
        
class ExampleForm(MyBaseForm):
    dt = DateField('DatePicker', format='%Y-%m-%d')


    
