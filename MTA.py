import psycopg2
import psycopg2.extras
from psycopg2 import pool
from passlib.hash import sha256_crypt
import settings
from datetime import datetime, timedelta

from psycopg2 import sql


# Vrati novu instanciu spojenia s DB serverom
def connect_db():
    conn = None
    try:
        connection_string = "dbname=%s user=%s host=%s password=%s port=%s" % (settings.DB_NAME, settings.DB_USERNAME, settings.DB_HOST, settings.DB_PASSWD, settings.DB_PORT)
        conn = psycopg2.connect(connection_string)
        return conn

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


# Vypise informacie od DB stroji, vhodne napr pre testovanie spojenia
def stats(connection):
    cur = connection.cursor()
    print('PostgreSQL database version:')
    cur.execute('SELECT version()')
    db_version = cur.fetchone()
    print(db_version)

#####################################

######### LOGIN ##############
#login overenie hesla
def log_in(connection, login, password_candidate):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    result = cur.execute('SELECT * FROM users WHERE email = %s',[login])
    data = cur.fetchone()
    
    if data:
        password = data['password']
        admin = data['user_roles_id']
        return password, admin
    else:
        return 

def user_exist(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    result = cur.execute('SELECT * FROM users WHERE name = %s',[login])
    data = cur.fetchone()
    
    if data:
        return True
    else:
        return 

#pridanie uzivatela
def add_user(connection, username, password, role, email):
    now = (datetime.now()+ timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S') 
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    try:
        cur.execute("""INSERT INTO users (name, email, password, registered, user_roles_id)
                    VALUES (%s, %s, %s, %s, %s)""",(username, email, password, now, role))
        connection.commit()
        cur.close()
        return True
    except (Exception, psycopg2.DatabaseError) as error:
        connection.rollback()
        cur.close()
        return False
    

#vytiahnutie roli
def get_roles(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT * FROM user_roles')
    data = cur.fetchall()
    return data

#kontrola, ci je username uz v appke
def check_username(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT * FROM user_roles')
    data = cur.fetchall()
    return data

#kontrola, ci je email uz v appke
def check_email(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT * FROM user_roles')
    data = cur.fetchall()
    return data



    
def get_email_by_login(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    result = cur.execute('SELECT email FROM users WHERE name = %s',[login])
    data = cur.fetchone()
    return data

def get_pass(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    result = cur.execute('SELECT password FROM users WHERE name = %s',[login])
    data = cur.fetchone()
    return data

def check_pass(connection, login, password_candidate):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    result = cur.execute('SELECT * FROM users WHERE name = %s',[login])
    data = cur.fetchone()
    return data

    if data:
        password = data['password']
        return password
    else:
        return 
########### PRODUKTY ###########

# Vrati zoznam vsetkych produktov a pocet recenzii ako list objektov vo formate json
def get_products(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(""" SELECT products_id, name, brand, model, introduced, url_img, url_web, COALESCE(rev_cnt.reviews_count, 0) reviews_count, product_categories_id
				 FROM products
				 LEFT JOIN (SELECT products_id, count(review_text) reviews_count FROM reviews GROUP BY products_id) rev_cnt USING (products_id)
				 ORDER BY name """)
    return cur.fetchall()



def example(connection, filter,value, table, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL("""SELECT reviews_id, reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id  )  AND reviews_suggestions_states_id=3 
                    AND {}.{} = %s AND suggestion_time > %s AND suggestion_time < %s
                    ORDER BY suggestion_time DESC""").format(sql.Identifier(table),sql.Identifier(filter)),(value, start_date, end_date))
    return cur.fetchall()


def example2(connection, filter,value, login, table, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL("""SELECT reviews_id, reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id  )  AND reviews_suggestions_states_id=3 
                    AND {}.{} = %s AND users_id = %s AND suggestion_time > %s AND suggestion_time < %s
                    ORDER BY suggestion_time DESC""").format(sql.Identifier(table),sql.Identifier(filter)),(value, login, start_date, end_date))
    return cur.fetchall()

def example3(connection, login, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL("""SELECT reviews_id, reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id  )  AND reviews_suggestions_states_id=3 
                    AND users_id = %s AND suggestion_time > %s AND suggestion_time < %s
                    ORDER BY suggestion_time DESC"""),(login, start_date, end_date))
    return cur.fetchall()

def example4(connection,start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL("""SELECT reviews_id, reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id  )  AND reviews_suggestions_states_id=3 
                    AND suggestion_time > %s AND suggestion_time < %s
                    ORDER BY suggestion_time DESC"""),(start_date, end_date))
    return cur.fetchall()

def example1(connection, filter,value):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    cur.execute(
    sql.SQL("SELECT * FROM reviews WHERE {} = %s")
        .format(sql.Identifier(filter)),
    [value])

    return cur.fetchall()

def get_rejected_reviews(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT r1.feature_names_id, r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE users_id = %s AND reviews_suggestions_states_id = 1
                    ORDER BY suggestion_time DESC""",[id_users])
    return cur.fetchall() 

def get_submited_reviews(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT  r1.feature_names_id, r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 2
                    ORDER BY suggestion_time DESC""",[id_users])
    return cur.fetchall() 

def get_suggested_reviews(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT  r1.feature_names_id, r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 3
                    ORDER BY suggestion_time DESC""",[id_users])
    return cur.fetchall() 

def get_returned_reviews(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT r1.feature_names_id, r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 4
                    ORDER BY reviews_id """,[id_users])
    return cur.fetchall() 

def get_returned_reviews_ext(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.feature_names_id , r1.feature_names_id , r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 4
                    ORDER BY reviews_id """,[id_users])
    return cur.fetchall() 

def get_returned_reviews_ext1(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.feature_names_id , r1.feature_names_id , r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 4
                    ORDER BY reviews_id """,[id_users])
    return cur.fetchall() 

def get_returned_reviews_for_modify(connection, id_users, reviews_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT languages_id, r1.feature_names_id, r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 4 AND reviews_id = %s
                    ORDER BY suggestion_time DESC""",(id_users, reviews_id))
    return cur.fetchone() 

def get_approved_reviews(connection, id_users):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT r1.feature_names_id, r1.reviews_suggestions_states_id stav, reviews.text popis, r1.sentiment, feature_names.text, czech_name, reviews.reviews_id  FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    LEFT JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    LEFT JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id = %s AND r1.reviews_suggestions_states_id = 5
                    ORDER BY suggestion_time DESC""",[id_users])
    return cur.fetchall() 

def get_untouched_reviews(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, feature_names.text, czech_name  FROM reviews
                    JOIN feature_names USING (feature_names_id)
                    JOIN product_categories USING (product_categories_id)
                    WHERE reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions)
                    ORDER BY reviews_id""")
    return cur.fetchall() 

#vytiahnutie recenzii
def get_reviews(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("SELECT * FROM reviews LIMIT 50")
    return cur.fetchall() 

def get_review_by_id(connection, review_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT * FROM reviews 
                    WHERE reviews_id = %s""",[review_id])
    return cur.fetchone() 

#vytiahnutie poctov navrhov
def get_count_of_suggestions(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(""" SELECT COUNT(r1.reviews_id), reviews_id, reviews.text, czech_name FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id ) AND reviews_suggestions_states_id=3
                    GROUP BY r1.reviews_id, reviews.text, czech_name""")
    return cur.fetchall() 

def get_count_of_suggestions_by_date(connection, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(""" SELECT COUNT(r1.reviews_id), reviews_id, reviews.text, czech_name FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id ) AND reviews_suggestions_states_id=3
                    AND suggestion_time > %s AND suggestion_time < %s
                    GROUP BY r1.reviews_id, reviews.text, czech_name""", (start_date, end_date))
    return cur.fetchall() 

def get_count_of_suggestions_by_date_filter(connection, start_date, end_date, table, filter, value):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL(""" SELECT COUNT(r1.reviews_id), reviews_id, reviews.text, czech_name, {}.{} FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id ) AND reviews_suggestions_states_id=3
                    AND suggestion_time > %s  AND suggestion_time <= %s AND {}.{} = %s
                    GROUP BY r1.reviews_id, reviews.text, czech_name, {}.{}""").format(sql.Identifier(table),sql.Identifier(filter),
                                                                                                   sql.Identifier(table),sql.Identifier(filter),sql.Identifier(table),
                                                                                                   sql.Identifier(filter)), (start_date, end_date, value))
    return cur.fetchall() 

def get_count_of_suggestions_by_date_filter1(connection, start_date, end_date, table, filter, value):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL(""" SELECT COUNT(r1.reviews_id), reviews_id, r2.text, czech_name, {}.{} FROM  reviews_suggestions r1
                    JOIN (SELECT feature_names_id, reviews_id, reviews.text,  feature_names.text feature FROM reviews JOIN feature_names USING(feature_names_id)) r2 USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id ) AND reviews_suggestions_states_id=3
                    AND suggestion_time > %s  AND suggestion_time <= %s AND {}.{} = %s
                    GROUP BY r1.reviews_id, r2.text, czech_name, {}.{}""").format(sql.Identifier(table),sql.Identifier(filter),
                                                                                                   sql.Identifier(table),sql.Identifier(filter),sql.Identifier(table),
                                                                                                   sql.Identifier(filter)), (start_date, end_date, value))
    return cur.fetchall() 

def get_count_of_suggestions_by_date_filter_ilike(connection, start_date, end_date, table, filter, value):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(
        sql.SQL(""" SELECT COUNT(r1.reviews_id), reviews_id, reviews.text, czech_name, {}.{} FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id ) AND reviews_suggestions_states_id=3
                    AND suggestion_time > %s  AND suggestion_time <= %s AND {}.{} ILIKE %s
                    GROUP BY r1.reviews_id, reviews.text, czech_name, {}.{}""").format(sql.Identifier(table),sql.Identifier(filter),
                                                                                                   sql.Identifier(table),sql.Identifier(filter),sql.Identifier(table),
                                                                                                   sql.Identifier(filter)), (start_date, end_date, value))
    return cur.fetchall() 

#vytiahnutie poctov navrhov
def get_count_of_suggestions_user(connection, users_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(""" SELECT COUNT(r1.reviews_id), reviews_id, reviews.text, czech_name FROM  reviews_suggestions r1
                    JOIN reviews USING (reviews_id)
                    JOIN feature_names ON r1.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories USING(product_categories_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id ) AND reviews_suggestions_states_id=3 AND users_id = %s
                    GROUP BY r1.reviews_id, reviews.text, czech_name""",[users_id])
    return cur.fetchall() 

def get_numbers_of_reviews1(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.reviews_id FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    ORDER BY reviews.reviews_id""",[id])
    return cur.fetchall() 

#TODO doladit
def get_reviews_features3(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    ORDER BY reviews.reviews_id""",[id])
    return cur.fetchall()

def get_reviews_features3_without_empty_model(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    ORDER BY reviews.reviews_id""",[id])
    return cur.fetchall()

#vytiahnutie zakladneho vypisu - vratene + neskontrolovane
def get_reviews_default(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 5
                    ORDER BY suggestion_time DESC )
                    ORDER BY reviews.reviews_id""",[id])
    return cur.fetchall()

def get_reviews_default_extend(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT x.reviews_suggestions_states_id stav, feature_names.feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    LEFT JOIN (SELECT reviews_suggestions_states_id,reviews_id, reviews_suggestions_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s) x ON x.reviews_id = reviews.reviews_id
                    WHERE model != '' AND reviews.reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews.reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id 
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                   OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    ORDER BY reviews.reviews_id """,(id,id))
    return cur.fetchall()

def get_reviews_default_extend_without_empty_model(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT x.reviews_suggestions_states_id stav, feature_names.feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    LEFT JOIN (SELECT reviews_suggestions_states_id,reviews_id, reviews_suggestions_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s) x ON x.reviews_id = reviews.reviews_id
                    WHERE reviews.reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews.reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id 
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                   OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    ORDER BY reviews.reviews_id """,(id,id))
    return cur.fetchall()

def get_reviews_default_extend_without_empty_model_extended(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT sug_sentiment, sug_feature, ciselko, x.reviews_suggestions_states_id stav, feature_names.feature_names_id, reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    LEFT JOIN (SELECT r1.feature_names_id ciselko, reviews_suggestions_states_id,reviews_id, reviews_suggestions_id, sentiment sug_sentiment, text sug_feature FROM  reviews_suggestions r1
                    JOIN feature_names ON feature_names.feature_names_id= r1.feature_names_id
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s) x ON x.reviews_id = reviews.reviews_id
                    WHERE reviews.reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews.reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id 
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                   OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    ORDER BY x.reviews_suggestions_states_id, reviews.reviews_id""",(id,id))
    return cur.fetchall()


#z navrhov
def get_reviews_features_by_features_names_id(connection,features_names_id, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT feature_names.text , reviews_id, reviews_suggestions_id, r1.suggestion_time, r1.sentiment, r1.feature_names_id, reviews_suggestions_states_id stav, reviews.text popis, czech_name FROM  reviews_suggestions r1
                JOIN reviews USING (reviews_id)
                JOIN feature_names ON feature_names.feature_names_id=r1.feature_names_id
                JOIN product_categories ON feature_names.product_categories_id= product_categories.product_categories_id
                WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                AND r1.users_id = reviews_suggestions.users_id ) 
                AND users_id = %s AND r1.feature_names_id = %s""",(id, features_names_id))
    return cur.fetchall() 

def get_reviews_features_by_features_names_id1(connection,sentiment, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT czech_name, reviews.sentiment, feature_names.text, feature_names.feature_names_id, reviews.text popis, reviews.reviews_id FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    AND feature_names.feature_names_id= %s
                    ORDER BY reviews.reviews_id""",(id,sentiment))
    return cur.fetchall() 

#z navrhov
def get_reviews_features_by_sentiment(connection,sentiment, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT feature_names.text , reviews_id, reviews_suggestions_id, r1.suggestion_time, r1.sentiment, r1.feature_names_id, reviews_suggestions_states_id stav, reviews.text popis, czech_name FROM  reviews_suggestions r1
                JOIN reviews USING (reviews_id)
                JOIN feature_names ON feature_names.feature_names_id=r1.feature_names_id
                JOIN product_categories ON feature_names.product_categories_id= product_categories.product_categories_id
                WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                AND r1.users_id = reviews_suggestions.users_id ) 
                AND users_id = %s and r1.sentiment = %s""",(id,sentiment))
    return cur.fetchall() 

def get_reviews_features_by_sentiment1(connection,sentiment, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT czech_name, reviews.sentiment, feature_names.text , reviews.text popis, reviews.reviews_id FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    AND sentiment = %s
                    ORDER BY reviews.reviews_id""",(id,sentiment))
    return cur.fetchall() 


def get_reviews_features_filtration_reviews_id(connection,id,search):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    AND reviews.reviews_id = %s
                    ORDER BY reviews.reviews_id LIMIT 10""",(id, search))
    return cur.fetchall() 

def get_reviews_features_filtration_feature(connection,id,search):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    AND feature_names.text = %s
                    ORDER BY reviews.reviews_id LIMIT 10""",(id, search))
    return cur.fetchall() 

def get_reviews_features_filtration_sentiment(connection,id,search):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.reviews_id, reviews.text popis, reviews.sentiment, product_categories.czech_name, feature_names.description, feature_names.text FROM reviews
                    JOIN products USING (products_id)
                    JOIN feature_names USING(feature_names_id)
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    WHERE model != '' AND reviews_id NOT IN (SELECT reviews_id FROM reviews_suggestions WHERE reviews_suggestions_states_id =5)AND reviews_id NOT IN (SELECT reviews_id FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id AND r1.users_id = reviews_suggestions.users_id)
                    AND users_id= %s
                    GROUP BY r1.reviews_suggestions_id
                    HAVING reviews_suggestions_states_id = 2 OR reviews_suggestions_states_id = 3 
                    OR reviews_suggestions_states_id = 1 
                    OR reviews_suggestions_states_id = 4 OR reviews_suggestions_states_id =5
                    ORDER BY suggestion_time DESC )
                    AND reviews.sentiment = %s
                    ORDER BY reviews.reviews_id LIMIT 10""",(id, search))
    return cur.fetchall() 

def get_numbers_of_submited_reviews(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT COUNT(*) pocet, a.reviews_id id FROM(SELECT * FROM reviews_suggestions 
                    WHERE  reviews_suggestions_states_id=2) a
                    GROUP BY a.reviews_id""")
    return cur.fetchall() 

def get_numbers_of_suggested_reviews(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT COUNT(*) pocet, a.reviews_id id FROM(SELECT * FROM reviews_suggestions 
                    WHERE  reviews_suggestions_states_id=3) a
                    GROUP BY a.reviews_id""")
    return cur.fetchall() 

def get_numbers_of_reviews(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT COUNT(*) pocet, a.reviews_id id, reviews_suggestions_states_id FROM(SELECT * FROM reviews_suggestions 
                    WHERE  reviews_suggestions_states_id=3 OR reviews_suggestions_states_id=2 ) a
                    GROUP BY a.reviews_id, reviews_suggestions_states_id
                    ORDER BY id""")
    return cur.fetchall() 

def get_reviews_features1(connection,login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(""" SELECT * FROM features
                LEFT JOIN control USING(features_id)
                WHERE (features_id, login) NOT IN 
                (SELECT features_id, login FROM control WHERE login = %s) """,[login])
    return cur.fetchall() 

#vytiahnutie mena uzivatela podla emailu
def get_user_by_email(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT name,email FROM users WHERE email= %s', [login])
    return cur.fetchone() 

#vytiahnutie recenzie s clustrami
def get_reviews_features(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(""" SELECT features.sentiment, review_text, features_text.text, features.features_id FROM features
                    JOIN (SELECT * FROM reviews JOIN products USING (products_id)) rev
                    ON features.reviews_id = rev.reviews_id
                    JOIN features_text USING (features_text_id)
                    WHERE features.features_id  NOT IN (SELECT features_id FROM control) OR (%s NOT IN (SELECT login FROM control)) 
                    ORDER BY features.features_id """,[login])
    return cur.fetchall() 

#akceptovanie navrhu
def accept_suggestion(connection, user_id, review_id, sentiment, attribute):
    now = (datetime.now()+ timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S') 
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""INSERT INTO reviews_suggestions (users_id,suggestion_time,reviews_id,reviews_suggestions_states_id,
                    sentiment, feature_names_id) 
                    VALUES(%s,%s,%s,5,%s,%s)""", (user_id, now, review_id, sentiment,attribute))
    connection.commit()
    cur.close()

#vytiahnutie vsetkych navrhov
def get_suggestions(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_id, reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id  )  AND reviews_suggestions_states_id=3
                    ORDER BY suggestion_time DESC""")
    return cur.fetchall() 

#vytiahnutie vsetkych navrhov
def get_suggestions_by_date(connection, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_id, reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id  )  AND reviews_suggestions_states_id=3
                    AND suggestion_time > %s AND suggestion_time < %s
                    ORDER BY suggestion_time DESC""", (start_date, end_date))
    return cur.fetchall() 

def get_suggestions_for_review(connection, reviews_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id 
                    AND abc.users_id = reviews_suggestions.users_id) AND reviews_suggestions_states_id=3
                    AND reviews_id=%s
                    ORDER BY suggestion_time DESC""",[reviews_id])
    return cur.fetchall() 

# neschvalene navrhy pre danu recenziu
def get_returned_suggestions_for_review(connection, reviews_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_suggestions_states_id, reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_states_id=4
                    AND reviews_id=%s
                    ORDER BY suggestion_time DESC""",[reviews_id])
    return cur.fetchall() 

#vytiahnutie recenzie 
def get_review(connection, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_id, reviews.text, sentiment, feature_names.text feature, description, czech_name, languages_id  FROM reviews
                    JOIN feature_names USING (feature_names_id)
                    JOIN product_categories USING (product_categories_id)
                    WHERE reviews_id = %s""", [id])
    return cur.fetchone() 

def get_review_ext(connection, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_id, reviews.text popis, sentiment, feature_names.text , description, czech_name, languages_id  FROM reviews
                    JOIN feature_names USING (feature_names_id)
                    JOIN product_categories USING (product_categories_id)
                    WHERE reviews_id = %s""", [id])
    return cur.fetchone() 

#vytiahnutie pocetnosti features pre danu recenziu
def get_sug_fea_for_review(connection, reviews_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT COUNT(text) pocet, text FROM(SELECT text FROM  reviews_suggestions r1
                    JOIN feature_names USING (feature_names_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id) 
                    AND reviews_id=%s AND reviews_suggestions_states_id = 3
                    ORDER BY suggestion_time DESC) abc
                    GROUP BY text
                    ORDER BY pocet DESC""",[reviews_id])
    return cur.fetchall() 

#vytiahnutie pocetnosti sentimentu pre danu recenziu
def get_sug_sent_for_review(connection, reviews_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT COUNT(sentiment) pocet, sentiment FROM(SELECT sentiment FROM  reviews_suggestions r1
                    JOIN feature_names USING (feature_names_id)
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id) 
                    AND reviews_id=%s  AND reviews_suggestions_states_id= 3 
                    ORDER BY suggestion_time DESC) abc
                    GROUP BY sentiment
                    ORDER BY pocet DESC""",[reviews_id])
    return cur.fetchall() 

#vytiahnutie rovnakych navrhov     
def get_correct_suggestions(connection, reviews_id, features_id, sentiment):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT * FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id) AND reviews_suggestions_states_id = 3 
                    AND reviews_id=%s
                    AND sentiment= %s AND feature_names_id = %s
                    ORDER BY suggestion_time DESC""",(reviews_id, sentiment, features_id))
    return cur.fetchall() 

#vytiahnutie navrhov, ktore sa nezhoduju
def get_incorrect_suggestions(connection, reviews_id, features_id, sentiment):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT * FROM  reviews_suggestions r1
                    WHERE reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE r1.reviews_id = reviews_suggestions.reviews_id 
                    AND r1.users_id = reviews_suggestions.users_id) 
                    AND reviews_suggestions_states_id = 3 AND reviews_id=%s
                    AND (sentiment != %s OR feature_names_id !=%s)
                    ORDER BY suggestion_time DESC""",(reviews_id, sentiment, features_id))
    return cur.fetchall() 


def get_review_for_edit(connection, reviews_suggestions_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT languages_id, users_id, reviews_id,reviews_suggestions.sentiment, feature_names.text sug_fea, reviews.text, czech_name, reviews_suggestions_id FROM reviews_suggestions 
                    JOIN feature_names USING(feature_names_id)
                    JOIN reviews USING(reviews_id)
                    JOIN products USING (products_id)
                    JOIN product_categories on products.product_categories_id=product_categories.product_categories_id
                    WHERE reviews_suggestions_id = %s""", [reviews_suggestions_id])
    return cur.fetchone() 

def get_review_for_edit_ext(connection, reviews_suggestions_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.feature_names_id, languages_id, users_id, reviews_id,reviews_suggestions.sentiment, feature_names.text feature , reviews.text, czech_name, reviews_suggestions_id  FROM reviews_suggestions 
                    JOIN reviews USING(reviews_id)
                    JOIN feature_names ON feature_names.feature_names_id=reviews.feature_names_id
                    JOIN products USING (products_id)
                    JOIN product_categories on products.product_categories_id=product_categories.product_categories_id
                    WHERE reviews_suggestions_id = %s

""", [reviews_suggestions_id])
    return cur.fetchone() 

def change_edited_review(connection, feature_names_id, sentiment, reviews_suggestions_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""UPDATE reviews_suggestions SET feature_names_id = %s, sentiment = %s WHERE reviews_suggestions_id = %s""",(feature_names_id, sentiment, reviews_suggestions_id))
    connection.commit()
    cur.close()

#vytiahnutie vsetkych navrhov
def get_suggestion(connection, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT * FROM suggestion JOIN reviews USING(reviews_id)
                    WHERE suggestion.suggestion_id = %s """,[id])
    return cur.fetchone() 

#potvrdenie recenzie, ze je spravne zaradena
def submit_review(connection, review_id, user_id, sentiment, feature_names_id):
    # timedalta je pouzity kvoli tomu, lebo vracalo to cas, ktory je o 2 hod mensi
    now = (datetime.now()+ timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S') 
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""INSERT INTO reviews_suggestions (users_id,suggestion_time,reviews_id,
                    reviews_suggestions_states_id, sentiment, feature_names_id) 
                    VALUES(%s,%s,%s,2,%s,%s)""", (user_id, now, review_id, sentiment, feature_names_id))
    connection.commit()
    cur.close()

def get_last_id(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT MAX(control_id) FROM control """)
    return cur.fetchone() 

#vymazanie control
def delete_control(connection, login, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""DELETE FROM control WHERE (features_id = %s) AND (login = %s)""",(id,login))
    connection.commit()
    cur.close()

#uprava clustru pri danej recenzii
def edit_features(connection, f_t_id, sen, id_r):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""UPDATE features SET features_text_id = %s, sentiment = %s WHERE reviews_id = %s""",(f_t_id,sen,id_r))
    connection.commit()
    cur.close()

#uprava navrhu
def change_suggestion(connection, f_t_id, sen, id_r):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""UPDATE features SET features_text_id = %s, sentiment = %s WHERE reviews_id = %s""",(f_t_id,sen,id_r))
    connection.commit()
    cur.close()

#uprava navrhu
def change_review(connection, feature_id, sentiment, review_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""UPDATE reviews SET feature_names_id = %s, sentiment = %s WHERE reviews_id = %s""",(feature_id,sentiment,review_id))
    connection.commit()
    cur.close()

#vyhladanie navrhu podla id-cka
def get_suggestion_byID(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT * FROM reviews_suggestions WHERE reviews_suggestions_id = %s',[id])
    return cur.fetchone()

#vytiahnutie navrhov od urciteho uzivatela
#TODO login je id_account nie meno

def get_suggestion_by_user(connection,login, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_suggestions_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.feature_names_id IS NOT NULL 
                    AND reviews_suggestions_id = (SELECT MAX(reviews_suggestions_id) FROM reviews_suggestions 
                    WHERE abc.reviews_id = reviews_suggestions.reviews_id AND abc.users_id = reviews_suggestions.users_id) AND reviews_suggestions_states_id = 3
                    AND users_id = %s AND suggestion_time > %s AND suggestion_time < %s
                    ORDER BY suggestion_time DESC""",(login, start_date, end_date)) 
    return cur.fetchall()

#vymazabue navrhu
def reject_suggestion(connection,users_id, reviews_id, sentiment, attribute):
    now = (datetime.now()+ timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S') 
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""INSERT INTO reviews_suggestions (users_id, suggestion_time, reviews_id, sentiment,
                    feature_names_id, reviews_suggestions_states_id)
                    VALUES (%s, %s, %s,%s, %s,1)""", (users_id, now, reviews_id, sentiment, attribute))
    connection.commit()
    cur.close()

#vymazabue navrhu
def return_suggestion(connection,users_id, reviews_id, sentiment, attribute):
    now = (datetime.now()+ timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S') 
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""INSERT INTO reviews_suggestions (users_id, suggestion_time, reviews_id, sentiment,
                    feature_names_id, reviews_suggestions_states_id)
                    VALUES (%s, %s, %s,%s, %s,4)""", (users_id, now, reviews_id, sentiment, attribute))
    connection.commit()
    cur.close()
    
#pridanie do zoznamu check
def add_suggestion(connection, id_login, id, sentiment, attribute):
    #timedalta je pouzity kvoli tomu, lebo vracalo to cas, ktory je o 2 hod mensi
    now = (datetime.now()+ timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S') 
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""INSERT INTO reviews_suggestions (users_id, suggestion_time, reviews_id, sentiment,
                    feature_names_id, reviews_suggestions_states_id)
                    VALUES (%s, %s, %s,%s, %s,3)""", (id_login, now, id, sentiment, attribute))
    connection.commit()
    cur.close()

#ziskanie product_id, reviews_id na zaklade review_text
def get_pi_ri(connection,rev):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT products_id, reviews_id FROM reviews WHERE review_text= %s',[rev])
    return cur.fetchone()

#vytiahnutie jazykov
def get_languages(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT languages_id, title_english FROM languages')
    return cur.fetchall() 

#vytiahnutie nazvov clustrov
def get_features_text(connection, product_categories_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT feature_names_id,features_id, text FROM feature_names
                    WHERE product_categories_id= %s
                    ORDER BY text''',[product_categories_id])
    return cur.fetchall() 

def get_features_text_in_language(connection, product_categories_id, languages_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT feature_names_id,features_id, text FROM feature_names
                    WHERE product_categories_id= %s AND languages_id = %s
                    ORDER BY text''',(product_categories_id, languages_id))
    return cur.fetchall() 

#vytiahnutie vsetkych nazvov clustrov
def get_all_features_text(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT feature_names_id, text FROM feature_names
                   ORDER BY text''')
    return cur.fetchall() 

#vytiahnutie vsetkych nazvov clustrov
def get_lang_features_text(connection, languages_id, category_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT feature_names_id, text, features_id, product_categories_id FROM feature_names
                   WHERE languages_id = %s AND product_categories_id = %s
                   ORDER BY text''', (languages_id, category_id))
    return cur.fetchall() 

def get_language_by_feature(connection, features_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT languages_id FROM feature_names
                   WHERE feature_names_id = %s''', [features_id])
    return cur.fetchone() 

#vytiahnutie vsetkych nazvov clustrov
def get_feature_by_lang_fea(connection, features_id, languages_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT feature_names_id FROM feature_names
                   WHERE features_id = %s and languages_id = %s''', (features_id,languages_id))
    return cur.fetchone() 

def get_feature_by_lang_fea_product_cat(connection, features_id, languages_id,product_categories_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT feature_names_id FROM feature_names
                   WHERE features_id = %s and languages_id = %s AND product_categories_id= %s''', (features_id,languages_id,product_categories_id))
    return cur.fetchone() 

def get_features_by_language(connection, product_categories_id, languages_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT * FROM feature_names
                   WHERE product_categories_id = %s and languages_id = %s
                   ORDER BY text''', (product_categories_id,languages_id))
    return cur.fetchall()

def get_features_by_features_id_lang(connection, features_id , languages_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT * FROM feature_names
                   WHERE features_id = %s and languages_id = %s ''', (features_id,languages_id))
    return cur.fetchone()

def get_features_by_features_id_lang_product_category(connection, features_id , languages_id, product_categories_id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT * FROM feature_names
                   WHERE features_id = %s and languages_id = %s AND product_categories_id = %s''', (features_id,languages_id,product_categories_id))
    return cur.fetchone()

#vytiahnutie nazvov clustrov
def get_category_id(connection, category):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT product_categories_id FROM product_categories
                    WHERE czech_name= %s''',[category])
    return cur.fetchone() 

#zmena role uzivatela
def change_user_data(connection, id, role):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('UPDATE users SET user_roles_id = %s WHERE users_id = %s',(role,id))
    connection.commit();
    cur.close()

def change_username(connection, id, username):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('UPDATE users SET name = %s WHERE users_id = %s',(username,id))
    connection.commit();
    cur.close()

#ziskanie product_id, reviews_id na zaklade review_text
def get_user_role(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT user_roles.name FROM user_roles
                    JOIN users USING (user_roles_id)
                    WHERE users.name= %s''',[login])
    return cur.fetchone()    

#vymazanie uzivatela
def delete_user(connection,id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('DELETE FROM users WHERE users_id = %s',[id])
    connection.commit();
    cur.close()

#zmena hesla
def change_password(connection, login, password):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('UPDATE users SET password = %s  WHERE name = %s',(password, login))
    connection.commit();
    cur.close()

#vytiahnutie loginu podla id_user
def get_login_byID(connection, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT name FROM users WHERE users_id = %s',[id])
    return cur.fetchone() 

#vytiahnutie is podla loginu
def get_id_by_login(connection, login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT users_id FROM users WHERE name = %s',[login])
    return cur.fetchone() 

#vytiahnutie is podla loginu
def get_id_by_email(connection, email):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('SELECT users_id FROM users WHERE email = %s',[email])
    return cur.fetchone() 

#vytiahnutie role podla id
def get_role_by_id(connection, id):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute('''SELECT user_roles.name FROM users 
                JOIN user_roles USING (user_roles_id)
                WHERE users_id = %s''',[id])
    return cur.fetchone() 

def get_evidence(connection,id_login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.text recenzia , reviews.sentiment , czech_name, feature_names.text, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id) WHERE reviews_suggestions_states_id = 2) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE users_id = %s
                    ORDER BY reviews_id""", [id_login])
    return cur.fetchall() 

def get_evidence_by_date(connection,id_login, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.text recenzia , reviews.sentiment , czech_name, feature_names.text, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id) WHERE reviews_suggestions_states_id = 2) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE users_id = %s and suggestion_time > %s AND suggestion_time < %s
                    ORDER BY reviews_id""", (id_login, start_date, end_date))
    return cur.fetchall() 

#vytiahnutie evidencie v ktorej bola zmenena feature
def get_changed_feature(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT abc.reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.text IS NOT NULL AND abc.text != feature_names.text
                    ORDER BY suggestion_time DESC""")
    return cur.fetchall() 

#vytiahnutie evidencie v ktorej bol zmeneny sentiment
def get_changed_sentiment(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT abc.reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.sentiment IS NOT NULL AND abc.sentiment != reviews.sentiment
                    ORDER BY suggestion_time DESC""")
    return cur.fetchall()

#vytiahnutie recenzii v ktorych boli zmenene aj feature aj sentiment
def get_changed_both(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT abc.reviews_suggestions_id, reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE abc.text IS NOT NULL AND abc.sentiment IS NOT NULL 
                    AND abc.sentiment != reviews.sentiment AND abc.text != feature_names.text
                    ORDER BY suggestion_time DESC""")
    return cur.fetchall()

#vytiahnutie evidencie od vsetkych uzivatelov
def get_all_evidence(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.text recenzia , reviews.sentiment , abc.sentiment zmena_sen, czech_name, feature_names.text, abc.text zmena_fea, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id)) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    ORDER BY suggestion_time DESC""")
    return cur.fetchall()

def get_evidence_sub_rev(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.text recenzia , reviews.sentiment , czech_name, feature_names.text, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id) WHERE reviews_suggestions_states_id = 2) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    ORDER BY reviews_id """)
    return cur.fetchall()

def get_evidence_sub_rev_by_date(connection, start_date, end_date):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT reviews.text recenzia , reviews.sentiment , czech_name, feature_names.text, suggestion_time, users.name, reviews_id FROM reviews
                    JOIN (SELECT * FROM reviews_suggestions LEFT JOIN feature_names USING (feature_names_id) WHERE reviews_suggestions_states_id = 2) abc USING (reviews_id) 
                    JOIN products USING (products_id)
                    LEFT JOIN feature_names ON reviews.feature_names_id = feature_names.feature_names_id
                    JOIN product_categories ON product_categories.product_categories_id = products.product_categories_id
                    JOIN users USING (users_id)
                    WHERE suggestion_time < %s AND suggestion_time > %s
                    ORDER BY reviews_id """,(end_date, start_date))
    return cur.fetchall()



def get_evidence_by_id_user(connection,login):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT control.login, control.time,review_text, features.sentiment , text, suggestion.sentiment suggsen, features.features_id idcko, id_account FROM control
                   JOIN account USING(login)
                   JOIN features USING (features_id)
                   JOIN features_text USING (features_text_id)
                   JOIN reviews USING(reviews_id)
                   LEFT JOIN suggestion USING(control_id)
                   WHERE id_account = %s """, [login])
    return cur.fetchall()
    
#vytiahnutie uzivatelov
def get_users(connection):
    cur = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute("""SELECT users_id, users.name, email, user_roles.name AS role FROM users 
                    JOIN user_roles USING (user_roles_id)
                    ORDER BY users_id""")
    return cur.fetchall() 

if __name__ == '__main__':
    connection = connect_db()
    print(stats(connection))
