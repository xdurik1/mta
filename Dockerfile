FROM python:3.9-slim-buster
RUN apt-get update -y
RUN apt-get install -y build-essential python3-dev
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN pip3 freeze
ENV PYTHONUNBUFFERED 0SSS